# Git for TFS users
Lab 04: Working with Pull Requests (squash merge + policies)

---

# Tasks

 - Creating a feature branch from visual studio
 
 - Link a work item from the commit message
 
 - Working with pull requests (squash merge + policies)

---

## Creating a feature branch from visual studio

 - Clone the repository "demo-app-lab-04" from visual studio:
    - In Team Explorer, open the Connect view
    - Select Clone under Local Git Repositories and enter the URL for your Git repo
    - Select a folder where you want your cloned repo to be kept
    - Select Clone to clone the repository

&nbsp;
<img alt="Image 1.0" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/clone_other_providers.png?view=azure-devops" border="1">
&nbsp;


 - In Team Explorer, select the Home icon and choose Branches:

&nbsp;
<img alt="Image 1.1" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/gitquickstart-vs2017/branches.png?view=azure-devops" border="1">
&nbsp;

 - Right click the master branch and choose New Local Branch From:

&nbsp;
<img alt="Image 1.2" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/gitquickstart-vs2017/new-local-branch-menu.png?view=azure-devops" border="1">
&nbsp;

 - Create a new branch called feature/your-username

---

## Link a work item from the commit message

 - Browse to the backlog and look for your bug ID under the PBI called "Lab-04"

&nbsp;
<img alt="Image 1.3" src="Images/1.1.PNG"  border="1">
&nbsp;

- In the feature branch create a new commit with the message below (using your Bug ID instead of 541):

```
Resolving bug #541
```

- In the feature branch create a new commit with the message below (using your Bug ID instead of 541):

```
Update bug #541
```

- In the feature branch create a new commit with the message below (using your Bug ID instead of 541):

```
Fixing bug #541
```

 - Push your commits and ensure that a new link was created in your work item

---

## Working with pull requests (squash merge + policies)

 - Open Pull Requests in Team Explorer

&nbsp;
<img alt="Image 1.4" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/gitquickstart-vs2017/pull-requests.png?view=azure-devops" border="1">
&nbsp;

 - Create a new Pull Request (from your feature into master)

&nbsp;
<img alt="Image 1.5" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/gitquickstart-vs2017/new-pull-request.png?view=azure-devops" border="1">
&nbsp;

 - Review your changes, ask someone to approve your changes and complete your pull request

